![stability-wip](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png) 
![openissues-wip](https://bitbucket.org/repo/ekyaeEE/images/2944199103-issues_open.png) 
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)


# RATIONALE #

* Path to follow on install OJS:
     - _offline_: for the sake of template design
	 - _online_: server side / production mode
* Implies some kind of checklist to follow
![TM25.png](https://bitbucket.org/repo/rpybXp8/images/3099136996-TM25.png)

> version 1
![temas_dos.png](https://bitbucket.org/repo/rpybXp8/images/829942321-temas_dos.png)

> version 2

![book-3D.png](https://bitbucket.org/repo/rpybXp8/images/2004126747-book-3D.png)

> Book 3D (needs final render)

![2018-08-06 14.33.00.gif](https://bitbucket.org/repo/rpybXp8/images/763418597-2018-08-06%2014.33.00.gif)

> Render

![BookPack 2018-08-30 14195100000.png](https://bitbucket.org/repo/rpybXp8/images/1319582231-BookPack%202018-08-30%2014195100000.png)

### What is this repository for? ###

* Quick summary
    - A kind of _checklist_ on the setup of an e-journal repository
* Version 1.01

### How do I get set up? ###

* Summary of set up
    - Vide [Colophon.md](https://bitbucket.org/imhicihu/open-journal-system-ojs-project/src/bc926daaa58527e7792d1f1b3e2b39442486b78a/Colophon.md?at=master&fileviewer=file-view-default)
* Configuration
    - Check our [installation](https://bitbucket.org/imhicihu/open-journal-system-ojs-project/src/master/Procedures/installation.md) guide.
* Dependencies
    - [Gulp](https://gulpjs.com/)
    - [NPM](https://www.npmjs.com/)
    - [Browsersync](https://browsersync.io/)
    
### Related repositories ###

* Some repositories linked with this project:
     - [Temas Medievales project](https://bitbucket.org/imhicihu/temas-medievales-project/src/)
     - [Scopus Metadata](https://bitbucket.org/imhicihu/scopus-metadata/src/)
    
### Source ###

* Check them on [here](https://bitbucket.org/imhicihu/open-journal-system-ojs-project/src)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/open-journal-system-ojs-project/issues?status=new&status=open)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/open-journal-system-ojs-project/commits/) section for the current status

### Sitemap ###
![OJS.jpg](https://bitbucket.org/repo/rpybXp8/images/2062653371-OJS.jpg)

### Roadmap ###
* _Vide_ [`tasks.md`](https://bitbucket.org/imhicihu/open-journal-system-ojs-project/src/master/tasks.md)

### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/open-journal-system-ojs-project/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/open-journal-system-ojs-project/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### License ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png)